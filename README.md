# FPT-Hospital

Project Semeter 4 - FPT Aptech

**Mọi người Clone về tạo branch từ dev để Code**

**1. Setup** <br/> 

*  Để setup cho Glassfish vào file `glassfish-resources.xml` để sửa user vs password : `fpt-hospital\Hospital\Hospital-ejb\src\conf\META-INF\glassfish-resources.xml`
*  Sửa xong nếu deloy bằng Netbean không được vào web host của Glassfish Add cái resource đó vào
  
**2. Coding** <br/> 
*  Code xong mọi người commit + push lên branch của mình sau vào [GitLab](https://gitlab.com/binlove9225/fpt-hospital) tạo Merge Request Từ Branch của mình vào branch `dev`
*  Anh Sang sẽ Check Merge Request và Merge vào Dev Sau đó Thông báo cho cả nhóm
*  Các thành viên còn lại hoặc là merge từ dev vào branch đang làm hoặc là tạo branch mới từ `dev` để tiếp tục
<br/>

***Khuyến cáo:***
*  Nên tạo branch theo đúng future đang làm vd: Branch `future-Login`
*  Nên tạo branch mới để tiếp tục làm việc sau khi tạo Merge Request


**SQL Script**
[Hospital SQL Script](https://www.codepile.net/pile/bvwM9YyG)