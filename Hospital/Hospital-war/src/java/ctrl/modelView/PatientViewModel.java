package ctrl.modelView;

import java.util.List;
import model.entities.MedicalReport;
import model.entities.Patient;
import model.entities.Prescription;

public class PatientViewModel {
    private int countAppointment;
    private int countAppointmentOK;
    private List<MedicalReport> medicalReport;
    private Patient patient;
    private List<Prescription> prescription;

    public int getCountAppointment() {
        return countAppointment;
    }

    public void setCountAppointment(int countAppointment) {
        this.countAppointment = countAppointment;
    }

    public int getCountAppointmentOK() {
        return countAppointmentOK;
    }

    public void setCountAppointmentOK(int countAppointmentOK) {
        this.countAppointmentOK = countAppointmentOK;
    }

    public List<MedicalReport> getMedicalReport() {
        return medicalReport;
    }

    public void setMedicalReport(List<MedicalReport> medicalReport) {
        this.medicalReport = medicalReport;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public List<Prescription> getPrescription() {
        return prescription;
    }

    public void setPrescription(List<Prescription> prescription) {
        this.prescription = prescription;
    }    
}
