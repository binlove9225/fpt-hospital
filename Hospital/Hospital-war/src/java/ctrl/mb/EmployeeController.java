package ctrl.mb;

import ctrl.mb.util.JsfUtil;
import ctrl.rep.interfaces.IEmployeeRep;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.ejb.EJB;
import model.entities.Employees;

@Named(value = "employeeController")
@SessionScoped
public class EmployeeController implements Serializable {

    @EJB
    private IEmployeeRep ie;
    private Employees current;
    
    public EmployeeController() {
    }

    public Employees getEmployee() {
        if (current == null) {
            current = new Employees();
        }
        return current;
    }

    public void setEmployee(Employees admin) {
        this.current = admin;
    }

//<editor-fold defaultstate="collapsed" desc="Public Method">
    public String login() {
        Employees resultLogin = ie.checklog(current);
        if (resultLogin == null) {
            JsfUtil.addErrorMessage("Unknown login, try again");
            current = new Employees();
            return null;
        } else {
            current = resultLogin;
            JsfUtil.addSession("employee", current);
            return "Index?faces-redirect=true";
        }
    }

    public String logout() {
        return "Login.xhtml";
    }

    public void removSession() {
        JsfUtil.addSession("employee", null);
    }
//</editor-fold>
}
