package ctrl.mb;

import static com.sun.faces.facelets.util.Path.context;
import ctrl.mb.util.JsfUtil;
import ctrl.rep.interfaces.*;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.*;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.model.*;
import javax.faces.validator.ValidatorException;

import model.entities.*;

@ManagedBean
@SessionScoped
public class EmployeeMB {

//<editor-fold defaultstate="collapsed" desc="Field">
    @EJB
    private IEmployeeRep ie;
    @EJB
    private IDepartmentRep id;
    private Employees current;
    private DataModel<Employees> items = null;
    private int selectedItemIndex;
    private boolean isCreate;
    private boolean isDoctor;
//<editor-fold defaultstate="collapsed" desc="Properties">

    public boolean isIsCreate() {
        return isCreate;
    }

    public void setIsCreate(boolean isCreate) {
        this.isCreate = isCreate;
    }

    public boolean isIsDoctor() {
        return isDoctor;
    }

    public void setIsDoctor(boolean isDoctor) {
        this.isDoctor = isDoctor;
    }

//</editor-fold>
//</editor-fold>
//<editor-fold defaultstate="collapsed" desc="Constructor">
    public EmployeeMB() {
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Private Method">
    private IEmployeeRep getAction() {
        return ie;
    }

    private DataModel createModel() {
        return new ListDataModel(isDoctor ? getAction().findAllDoctor() : getAction().findAffStaff());
    }

    private void recreateModel() {
        items = null;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Public Method">
    public List<Employees> getDoctors() {
        return getAction().findAllDoctor();
    }

    public String find(int id) {
        return getAction().find(id).getEmployeeName();
    }

    public Employees findID(int id) {
        return getAction().find(id);
    }

    public DataModel getItems() {
        if (items == null) {
            items = createModel();
        }
        return items;
    }

    public Employees getByID(Object id) {
        return getAction().find(id);
    }

    public Employees getSelected() {
        if (current == null) {
            current = new Employees();
            selectedItemIndex = -1;
        }
        return current;
    }

    public String prepareListDoctor() {
        recreateModel();
        isDoctor = true;
        return "Employees?faces-redirect=true";
    }

    public String prepareSelectListDoctor() {
        recreateModel();
        isDoctor = true;
        return "DoctorSelect?faces-redirect=true";
    }

    public String prepareListStaff() {
        recreateModel();
        isDoctor = false;
        return "Employees?faces-redirect=true";
    }

    public String prepareCreate() {
        current = new Employees();
        current.setEmployeeID(0);
        isCreate = true;
        return "EmployeesForm";
    }

    public String prepareEdit(Object id) {
        current = getByID(id);
        isCreate = false;
        return "EmployeesForm";
    }

    public String resetPassword(Object id) {
        Employees emp = getByID(id);
        try {
            emp.setPassword("1");
            ie.edit(emp);
            JsfUtil.addSuccessMessage("Changed Password of " + emp.getEmployeeName() + " to '1'.");
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, "Reset Fail !");
        }
        return "Employees";
    }

    public String create() {
        try {
            getAction().create(current);
            JsfUtil.addSuccessMessage("Thêm thành công");
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, "Thêm thất bại");
            return null;
        }
    }

    public String update() {
        try {
            getAction().edit(current);
            JsfUtil.addSuccessMessage("Cập nhật thành công");
            return prepareEdit(current.getEmployeeID());
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, "Cập nhật thất bại");
            return null;
        }
    }

    public Collection<Department> getRefDepartment() {
        return id.findAll();
    }

    public Collection<Employees> getRefemployeeses() {
        return ie.findAllDoctor();
    }

    public void validateSamePassword(FacesContext context, UIComponent toValidate, Object value) {
        String confirmPassword = (String) value;
        if (!confirmPassword.equals(current.getPassword())) {
            FacesMessage message;
            message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Passwords do not match!", "Passwords do not match!");
            throw new ValidatorException(message);
        }
    }

    public void validateEmailExit(FacesContext context, UIComponent toValidate, Object value) {
        String email = (String) value;
        if (ie.checkEmployeeEmail(email)) {
            FacesMessage message;
            message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Email is Exits !", "Email is Exits !");
            throw new ValidatorException(message);
        }
    }
//</editor-fold>
}
