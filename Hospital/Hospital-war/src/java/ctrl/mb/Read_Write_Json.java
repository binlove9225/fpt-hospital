/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ctrl.mb;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.entities.Employees;
import model.entities.Patient;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author lenovo
 */
public class Read_Write_Json {
    
    public String path="F:\\fpt-hospital\\Hospital\\Hospital-war\\src\\java\\ctrl\\json\\";
    
    public List<JSONObject> getJsonTime() {
        JSONParser parse = new JSONParser();
        List<JSONObject> listTime = new ArrayList<>();
        try {
            Object obj = parse.parse(new FileReader(path+"AppointmentTime.json"));
            JSONArray jo = (JSONArray) obj;
            for (int i = 0; i < jo.size(); i++) {
                JSONObject o = (JSONObject) jo.get(i);
                if (o.get("status").equals("active")) {
                    listTime.add(o);
                }
            }
        } catch (IOException | org.json.simple.parser.ParseException ex) {
            Logger.getLogger(Read_Write_Json.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listTime;
    }

    public List<JSONObject> readJson() {
        JSONParser parse = new JSONParser();
        List<JSONObject> listTime = new ArrayList<>();
        try {
            Object obj = parse.parse(new FileReader(path+"AppointmentTime.json"));
            JSONArray jo = (JSONArray) obj;
            for (int i = 0; i < jo.size(); i++) {
                JSONObject o = (JSONObject) jo.get(i);
                listTime.add(o);
            }
        } catch (IOException | org.json.simple.parser.ParseException ex) {
            Logger.getLogger(Read_Write_Json.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listTime;
    }

    public void WriteJson(String time) {
        FileWriter file = null;
        try {
            List<JSONObject> list = readJson();
            JSONArray l = new JSONArray();
            for (int i = 0; i < list.size(); i++) {
                JSONObject obj = (JSONObject) list.get(i);
                if (obj.get("time").equals(time)) {
                    obj.put("status", "deactive");
                }
                l.add(obj);
            }
            file = new FileWriter(path+"AppointmentTime.json");
            file.write(l.toJSONString());
            file.close();
        } catch (IOException ex) {
            Logger.getLogger(Read_Write_Json.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void WritePreAppointment(String timeset,String date, String time, int room, int departmentId, Employees doctorID, Patient patientId) {
        String[] e_time=timeset.split(":");
        String expired_time=String.valueOf(Integer.parseInt(e_time[0])+3)+":"+e_time[1];
        Date d=new Date();
        String data = "[]"; 
        FileWriter writer=null;
        JSONParser parse = new JSONParser();
        try {
            File file = new File(path  + "PreAppointment.json");
            if (file.createNewFile()) {
                Files.write(Paths.get(file.getPath()), data.getBytes());
            }
            Object o= parse.parse(new FileReader(file.getPath()));
            JSONObject obj = new JSONObject();
            obj.put("id", d.getTime());
            obj.put("Book_time", timeset);
            obj.put("expired_time", expired_time);
            obj.put("date", date);
            obj.put("time", time);
            obj.put("room", room);
            obj.put("department", departmentId);
            obj.put("doctorId", doctorID.getEmployeeID());
            obj.put("patientId", patientId.getPatientID());
            obj.put("status","Deactive");
            JSONArray array = (JSONArray)o;
            array.add(obj);
            writer = new FileWriter(file.getPath());
            writer.write(array.toJSONString());
            writer.close();
        } catch (IOException | org.json.simple.parser.ParseException ex) {
            Logger.getLogger(Read_Write_Json.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public List<JSONObject> read_pre_app(){
        JSONParser parse = new JSONParser();
        List<JSONObject> listTime = new ArrayList<>();
        try {
            Object obj = parse.parse(new FileReader(path+"PreAppointment.json"));
            JSONArray jo = (JSONArray) obj;
            for (int i = 0; i < jo.size(); i++) {
                JSONObject o = (JSONObject) jo.get(i);
                listTime.add(o);
            }
        } catch (IOException | org.json.simple.parser.ParseException ex) {
            Logger.getLogger(Read_Write_Json.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listTime;
    }
    
    public void change_status(long id,String date){
        FileWriter ws=null;
        List<JSONObject> list =read_pre_app();
        JSONArray array=new JSONArray();
            for (int i = 0; i < list.size(); i++) {
                JSONObject obj=list.get(i);
                if(Long.parseLong(obj.get("id").toString())!=id){
                    array.add(obj);
                }
            }
        try {  
            ws=new FileWriter(path+"PreAppointment.json");
            ws.write(array.toJSONString());
            ws.close();
        } catch (IOException ex) {
            Logger.getLogger(Read_Write_Json.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void add_schedule(String filename,JSONObject obj){
        File file=new File(path+filename+".json");
        FileWriter ws=null;
        JSONArray array=(JSONArray)read_json(file.getPath());
        try {
            array.add(obj);
            ws=new FileWriter(file.getPath());
            ws.write(array.toJSONString());
            ws.close();
        } catch (IOException ex) {
            Logger.getLogger(Read_Write_Json.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public List<Object> read_json(String filename){
        FileReader reader=null;
        JSONArray array= new JSONArray();
        try {
            JSONParser parse=new JSONParser();
            reader=new FileReader(filename);
            array=(JSONArray)parse.parse(reader);
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Read_Write_Json.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException | ParseException ex) {
            Logger.getLogger(Read_Write_Json.class.getName()).log(Level.SEVERE, null, ex);
        }
        return array;
    }
}
