package ctrl.mb;

import ctrl.mb.util.JsfUtil;
import ctrl.modelView.PatientViewModel;
import ctrl.rep.interfaces.IMedicalReportRep;
import ctrl.rep.interfaces.IPatientRep;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.validator.ValidatorException;
import model.entities.MedicalReport;

import model.entities.Patient;
import model.entities.Prescription;

@ManagedBean
@SessionScoped
public class PatientMB {

//<editor-fold defaultstate="collapsed" desc="Field">
    @EJB
    private IPatientRep ip;
    @EJB
    private IMedicalReportRep im;

    private PatientViewModel patientView = null;
    private Patient current;
    private DataModel<Patient> items = null;
    private int selectedItemIndex;
    private boolean isCreate;
    private Patient patient;

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public boolean isIsCreate() {
        return isCreate;
    }

    public void setIsCreate(boolean isCreate) {
        this.isCreate = isCreate;
    }

//</editor-fold>
    public PatientMB() {
    }

//<editor-fold defaultstate="collapsed" desc="Private Method">
    // Cái này em dùng như vầy là để lúc a copy qua thằng khác chỉ cần sửa cái return, mấy hàm phía dưới ko bị ảnh hưởng
    private IPatientRep getAction() {
        return ip;
    }

    // Tạo Model
    private DataModel createModel() {
        return new ListDataModel(getAction().findAll());
    }

    // Gọi hàm này thì items = null mà ở page jsf gọi thằng getItems() thằng này sẽ check if null thì khởi tạo.
    private void recreateModel() {
        items = null;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Public Method">
    public Collection<Patient> getRefPatient() {
        return ip.findAll();
    }

    public List<Patient> getAll() {
        return getAction().findAll();
    }

    public DataModel getItems() {
        if (items == null) {
            items = createModel();
        }
        return items;
    }

    public Patient getByID(Object id) {
        return getAction().find(id);
    }

    public Patient getSelected() {
        if (current == null) {
            current = new Patient();
        }
        return current;
    }

    public String prepareList() {
        recreateModel();
        return "Patient?faces-redirect=true";
    }

    public String prepareCreate() {
        current = new Patient();
        current.setPatientID(0);
        isCreate = true;
        return "PatientForm";
    }

    public String prepareEdit(Object id) {
        current = getByID(id);
        isCreate = false;
        return "PatientForm";
    }

    public String prepareView(Object id) {
        patientView = new PatientViewModel();
        current = getByID(id);
        patientView.setPatient(current);
        patientView.setMedicalReport(im.findByPatient(current));
        patientView.setCountAppointment(current.getAppointmentList().size());
        patientView.setCountAppointmentOK(current.getAppointmentList().stream().filter(x -> x.getStatus().equals("Completed")).collect(Collectors.toList()).size());
        return "PatientView";
    }

    public String create() {
        try {
            getAction().create(current);
            JsfUtil.addSuccessMessage("Thêm bệnh nhân thành công");
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, "Thêm bệnh nhân thất bại");
            return null;
        }
    }

    public String update() {
        try {
            getAction().edit(current);
            JsfUtil.addSuccessMessage("Cập nhật bệnh nhân thành công");
            return prepareEdit(current.getPatientID());
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, "Cập nhật bệnh nhân thất bại");
            return null;
        }
    }

    public void validateCmndExit(FacesContext context, UIComponent toValidate, Object value) {
        String cmnd = (String) value;
        if (ip.checkCMND(cmnd)) {
            FacesMessage message;
            message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Patient is Exits !", "Patient is Exits !");
            throw new ValidatorException(message);
        }
    }

    public PatientViewModel getPatientView() {
        return patientView;
    }

    public void loadPrescription(MedicalReport entity) {
        patientView.setPrescription(entity.getPrescriptionCollection().stream().collect(Collectors.toList()));
    }
//</editor-fold>
}
