package ctrl.mb;

import ctrl.mb.util.JsfUtil;
import ctrl.rep.interfaces.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.*;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import model.entities.*;

@ManagedBean(name = "doctorMB")
@SessionScoped
public class DoctorController {

//<editor-fold defaultstate="collapsed" desc="Field">
    @EJB
    private IEmployeeRep _employeeRep;
    @EJB
    private IAppointmentRep _appointmentRep;
    @EJB
    private IMedicalReportRep _medicalReportRep;
    @EJB
    private IPrescriptionRep _prescriptionRep;
    @EJB
    private IRoomRep _roomRep;
    @EJB
    private ISickness _sickNessRep;
    @EJB
    private IMedicineRep _medicineRep;

    private final Employees currentDoctor;
    private Appointment currentAppointment;
    private MedicalReport currentMedicalReport;
    private Prescription currentPrescription;
    private Patient viewPatient;

    private DataModel<Appointment> itemsAppointment = null;
    private DataModel<Appointment> itemsAppointmentForMe = null;

//</editor-fold>
    public DoctorController() {
        currentDoctor = JsfUtil.getSession("employee") == null ? null : (Employees) JsfUtil.getSession("employee");
    }

//<editor-fold defaultstate="collapsed" desc="Private Method">
    private DataModel createAppointmentModel() {
        return new ListDataModel(_appointmentRep.findByDepartment(currentDoctor.getDepartmentID().getDepartmentID(), "NotComplete"));
    }

    private DataModel createAppointmentForMeModel() {
        return new ListDataModel(_appointmentRep.findByDoctor(currentDoctor, "NotComplete"));
    }

    private DataModel createMedicalReportModel() {
        return new ListDataModel(_medicalReportRep.findAll());
    }

    private DataModel createPrescriptionModel() {
        return new ListDataModel(_prescriptionRep.findAll());
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Public Method">
    public DataModel getItemsAppointment() {
        if (itemsAppointment == null) {
            itemsAppointment = createAppointmentModel();
        }
        return itemsAppointment;
    }

    public DataModel getItemsAppointmentForMe() {
        if (itemsAppointmentForMe == null) {
            itemsAppointmentForMe = createAppointmentForMeModel();
        }
        return itemsAppointmentForMe;
    }

    public String prepareList() {
        itemsAppointment = null;
        itemsAppointmentForMe = null;
        return "Doctor?faces-redirect=true";
    }

    public String prepareMedicalReport() {
        currentMedicalReport = new MedicalReport();
        currentMedicalReport.setDoctorID(currentAppointment.getDoctorID());
        currentMedicalReport.setPatientID(currentAppointment.getPatientID());
        return "MedicalReport";
    }

    public Appointment getByID(Object id) {
        return _appointmentRep.find(id);
    }

    public void updateAppointment(Appointment entity) {
        this.currentAppointment = getByID(entity.getAppointmentID());
        entity.setStatus("Completed");
        entity.setDoctorID(currentDoctor);
        _appointmentRep.edit(entity);
        this.currentAppointment = entity;
    }

//<editor-fold defaultstate="collapsed" desc="Load Ref Data">
    public List<Room> loadRefRoom() {
        return _roomRep.findAll();
    }

    public List<Sickness> loadRefSickness() {
        return _sickNessRep.findAll();
    }

    public List<Medicine> loadRefMedicine() {
        return _medicineRep.findAll();
    }
//</editor-fold>

    public void Refesh() {
        itemsAppointment = null;
        itemsAppointmentForMe = null;
    }

    public Appointment getAppointmentSelect() {
        if (currentAppointment == null) {
            currentAppointment = new Appointment();
        }
        return currentAppointment;
    }

    public MedicalReport getMedicalReportSelect() {
        if (currentMedicalReport == null) {
            currentMedicalReport = new MedicalReport();
        }
        if (currentMedicalReport.getPrescriptionCollection() == null) {
            currentMedicalReport.setPrescriptionCollection(new ArrayList<>());
        }
        return currentMedicalReport;
    }

    public Prescription getPrescriptionSelect() {
        if (currentPrescription == null) {
            currentPrescription = new Prescription();
            currentPrescription.setMedicineID(_medicineRep.find(1));
        }
        return currentPrescription;
    }

    public Patient getViewPatient() {
        if (viewPatient == null) {
            viewPatient = new Patient();
        }
        return viewPatient;
    }

    public void getViewPatient(Patient entity) {
        viewPatient = entity;
    }

//<editor-fold defaultstate="collapsed" desc="Events">
    public void changeDetail(ValueChangeEvent event) {
        currentPrescription.setNote(event.getNewValue().toString());
    }

    public void changeQuantity(ValueChangeEvent event) {
        currentPrescription.setTotal(Double.parseDouble(event.getNewValue().toString()));
    }

    public void changeMorning(ValueChangeEvent event) {
        currentPrescription.setMorning(Boolean.parseBoolean(event.getNewValue().toString()));
    }

    public void changeAfternoon(ValueChangeEvent event) {
        currentPrescription.setAfternoon(Boolean.parseBoolean(event.getNewValue().toString()));
    }

    public void changeEvening(ValueChangeEvent event) {
        currentPrescription.setEvening(Boolean.parseBoolean(event.getNewValue().toString()));
    }

    public void changeMedicine(ValueChangeEvent event) {
        currentPrescription.setMedicineID((Medicine) event.getNewValue());
    }

    public boolean saveEvent() {

        return true;
    }
//</editor-fold>

    public String updateMedicalReport() {
        currentPrescription.setPrescriptionID(0);
        Collection<Prescription> x = getMedicalReportSelect().getPrescriptionCollection();
        x.add(currentPrescription);
        currentPrescription = null;
        return null;
    }

    public void prepareEditPrescription(Prescription item) {
        currentPrescription = item;
        currentMedicalReport.getPrescriptionCollection().remove(item);
    }

    public void prepareRemovePrescription(Prescription item) {
        currentMedicalReport.getPrescriptionCollection().remove(item);
    }

    public String save() {
        try {
            currentMedicalReport.setReportID(0);
            currentMedicalReport.setDate(new Date());
            currentMedicalReport.getPrescriptionCollection().forEach((Prescription item) -> {
                item.setReportID(currentMedicalReport);
            });

            _medicalReportRep.create(currentMedicalReport);

            JsfUtil.addSuccessMessage("Tạo hồ sơ thành công");
            return "MedicalReport";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, "Tạo hồ sơ thất bại");
            return null;
        }
    }
//</editor-fold>
}
