/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ctrl.mb;

import ctrl.mb.util.JsfUtil;
import ctrl.rep.interfaces.IAppointmentRep;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import model.entities.Appointment;
import model.entities.Department;
import model.entities.Employees;
import model.entities.Patient;
import org.json.simple.JSONObject;

/**
 *
 * @author lenovo
 */
@ManagedBean
@SessionScoped
public class AppointmentMB_demo {

    //<editor-fold defaultstate="collapsed" desc="Field">
    @EJB
    private IAppointmentRep ip;
    private Appointment current;
    private DataModel<Appointment> items = null;
    private Read_Write_Json json = new Read_Write_Json();
    private String status;
    private int selectedItemIndex;
    private boolean isCreate;
    private String today;
    private String minday;
    private Patient patient;
    private List<String> listTime;
    private List<String> listDepartment;
    private int doctor;
    
    public boolean isIsCreate() {
        return isCreate;
    }

    public void setIsCreate(boolean isCreate) {
        this.isCreate = isCreate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getToday() {
        SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd");
        Date date=new Date();
        Date day= new Date(date.getTime()+Long.parseLong("1209600000"));
        return format.format(day);
    }

    public void setToday(String today) {
        this.today = today;
    }
    
    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public List<String> getListTime() {
        return listTime;
    }

    public void setListTime(List<String> listTime) {
        this.listTime = listTime;
    }

    public int getDoctor() {
        return doctor;
    }

    public void setDoctor(int doctor) {
        this.doctor = doctor;
    }

    public List<String> getListDepartment() {
        return listDepartment;
    }

    public void setListDepartment(List<String> listDepartment) {
        this.listDepartment = listDepartment;
    }
    
//</editor-fold>

    public AppointmentMB_demo() {
    }

    //<editor-fold defaultstate="collapsed" desc="Private Method">
    // Cái này em dùng như vầy là để lúc a copy qua thằng khác chỉ cần sửa cái return, mấy hàm phía dưới ko bị ảnh hưởng
    private IAppointmentRep getAction() {
        return ip;
    }

    // Tạo Model
    private DataModel createModel() {
        return new ListDataModel(getAction().findAll());
    }

    // Gọi hàm này thì items = null mà ở page jsf gọi thằng getItems() thằng này sẽ check if null thì khởi tạo.
    private void recreateModel() {
        items = null;
    }
//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Public Method">
    public List<Appointment> getAll() {
        return getAction().findAll();
    }

    public void search_pre_app() {
        read_pre_app();
    }

    public List<JSONObject> read_pre_app() {
        List<JSONObject> list = new ArrayList<>();
        list.addAll(json.read_pre_app());
        return list;
    }

    public void change_form(){
        EmployeeMB emp=new  EmployeeMB();
        DepartmentMB dMB=new DepartmentMB();
        //Employees e= emp.getByID(current.getDoctorID().getEmployeeID());
        int a=current.getDoctorID().getEmployeeID();
        current.setDoctorID(current.getDoctorID());
        current.setDepartmentID(current.getDoctorID().getDepartmentID().getDepartmentID());
        readJsonTime();
    }
    
    public void readJsonTime() {
        
        List<String> list;
            list=getAction().getTime(current.getDate(),new Employees(current.getDoctorID().getEmployeeID()));
        List<JSONObject> jsonlist= json.readJson();
        listTime=new ArrayList<>();
        for (JSONObject obj : jsonlist) {
            int exist=0;
            for (String a : list) {
                if(obj.get("time").toString().equals(a.toString())){
                    exist++;
                    break;
                }
            }
            if(exist==0){
                listTime.add(obj.get("time").toString());
            }
        }
    }

    public void add_appointment_normal() {
        int count = getAction().findbyDepartment(current.getDepartmentID(), current.getDate());
        if (count < 50) {
            current.setStatus("NotComplete");
            getAction().create(current);
            prepareCreateNomal();
        }
        else{
        JsfUtil.addErrorMessage("chuyên khoa đã đầy");
        }
        prepareCreateNomal();
    }

    public String change_pre_app(JSONObject item) {
        Appointment appCreate = new Appointment();
        appCreate.setDate(item.get("date").toString());
        appCreate.setTime(item.get("time").toString());
        appCreate.setDepartmentID(Integer.parseInt(item.get("department").toString()));
        appCreate.setPatientID(new Patient(Integer.parseInt(item.get("patientId").toString())));
        appCreate.setDoctorID(new Employees(Integer.parseInt(item.get("doctorId").toString())));
        appCreate.setRoomNo(Integer.parseInt(item.get("room").toString()));
        appCreate.setStatus("NotComplete");
        int existed = getAction().findByDateTime(appCreate.getDate(), appCreate.getTime(), new Employees(appCreate.getDoctorID().getEmployeeID()));
        if (existed != 0) {
            JsfUtil.addSuccessMessage("Thêm đặt hẹn thành công");
            return prepareCreate();
        }
        getAction().create(appCreate);
        json.change_status(Long.parseLong(item.get("id").toString()), item.get("date").toString());
        return "Appointment";
    }

    public DataModel getItems() {
        if (items == null) {
            items = createModel();
        }
        return items;
    }

    public Appointment getByID(Object id) {
        return getAction().find(id);
    }

    public Appointment getSelected() {
        if (current == null) {
            current = new Appointment();
            selectedItemIndex = -1;
        }
        return current;
    }

    public String prepareList() {
        recreateModel();
        return "Appointment?faces-redirect=true";
    }

    public String preparePreAppointmentList() {
        recreateModel();
        return "PreAppointmentIndex?faces-redirect=true";
    }

    public String prepareCreate() {
        //current = new Appointment();
        //current.setAppointmentID(0);
        isCreate = true;
        return "AppointmentForm";
    }

    public String prepareCreateNomal() {
        return "AppointmentFormNormal";
    }

    public String prepareEdit(Object id) {
        current = getByID(id);
        isCreate = false;
        return "AppointmentForm";
    }

    public String create() {
        try {
            int existed = getAction().findByDateTime(current.getDate(), current.getTime(), new Employees(current.getDoctorID().getEmployeeID()));
            if (existed != 0) {
                JsfUtil.addSuccessMessage("Thêm đặt hẹn thành công");
                return prepareCreate();
            }
            if (current.getStatus().equals("Pending")) {
                current.setStatus("Pending");
                getAction().create(current);
                JsfUtil.addSuccessMessage("Giữ chỗ thành công");
                return preparePreAppointmentList();
            }
            current.setStatus("NotComplete");
            boolean b= getAction().create(current);
            JsfUtil.addSuccessMessage("Thêm đặt hẹn thành công");
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, "Thêm đặt hẹn thất bại");
            return null;
        }
    }

    public String update() {
        try {
            getAction().edit(current);
            JsfUtil.addSuccessMessage("Cập nhật đặt hẹn thành công");
            return prepareEdit(current.getPatientID());
        } catch (Exception e) {
//            JsfUtil.addErrorMessage(e, "Cập nhật đặt hẹn thất bại");
            return null;
        }
    }
    public List<String> readTime(){
        List<JSONObject> jsonlist= json.readJson();
        List<String> list=new ArrayList<>();
        for (JSONObject jSONObject : jsonlist) {
            list.add(jSONObject.get("time").toString());
        }
        return list;
    }
//</editor-fold>
}
