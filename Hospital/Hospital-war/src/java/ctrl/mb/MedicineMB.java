
package ctrl.mb;

import ctrl.mb.util.JsfUtil;
import ctrl.rep.interfaces.*;
import java.util.*;
import javax.ejb.EJB;
import javax.faces.bean.*;
import javax.faces.model.*;

import model.entities.*;

@ManagedBean
@SessionScoped
public class MedicineMB {

//<editor-fold defaultstate="collapsed" desc="Field">
    @EJB
    private IMedicineRep im;
    private Medicine current;
    private DataModel<Medicine> items = null;
    private int selectedItemIndex;
    private boolean isCreate;

    public boolean isIsCreate() {
        return isCreate;
    }

    public void setIsCreate(boolean isCreate) {
        this.isCreate = isCreate;
    }

//</editor-fold>
    
    public MedicineMB() {
    }

//<editor-fold defaultstate="collapsed" desc="Private Method">
    // Cái này em dùng như vầy là để lúc a copy qua thằng khác chỉ cần sửa cái return, mấy hàm phía dưới ko bị ảnh hưởng
    private IMedicineRep getAction() {
        return im;
    }

    // Tạo Model
    private DataModel createModel() {
        return new ListDataModel(getAction().findAll());
    }

    // Gọi hàm này thì items = null mà ở page jsf gọi thằng getItems() thằng này sẽ check if null thì khởi tạo.
    private void recreateModel() {
        items = null;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Public Method">
    public DataModel getItems() {
        if (items == null) {
            items = createModel();
        }
        return items;
    }

    public Medicine getByID(Object id) {
        return getAction().find(id);
    }

    public Medicine getSelected() {
        if (current == null) {
            current = new Medicine();
            selectedItemIndex = -1;
        }
        return current;
    }

    public String prepareList() {
        recreateModel();
        return "Medicine?faces-redirect=true";
    }

    public String prepareCreate() {
        current = new Medicine();
        current.setMedicineID(0);
        isCreate = true;
        return "MedicineForm";
    }

    public String prepareEdit(Object id) {
        current = getByID(id);
        isCreate = false;
        return "MedicineForm";
    }

    public String create() {
        try {
            getAction().create(current);
            JsfUtil.addSuccessMessage("Thêm thành công");
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, "Thêm thất bại");
            return null;
        }
    }

    public String update() {
        try {
            getAction().edit(current);
            JsfUtil.addSuccessMessage("Cập nhật thành công");
            return prepareEdit(current.getMedicineID());
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, "Cập nhật thất bại");
            return null;
        }
    }
//</editor-fold>
}
