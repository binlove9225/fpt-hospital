package ctrl.mb;

import ctrl.mb.util.JsfUtil;
import ctrl.rep.interfaces.*;
import java.util.*;
import javax.ejb.EJB;
import javax.faces.bean.*;
import javax.faces.model.*;

import model.entities.*;

@ManagedBean
@SessionScoped
public class RoomMB {

//<editor-fold defaultstate="collapsed" desc="Field">
    @EJB
    private IRoomRep ir;
    @EJB
    private IDepartmentRep id;
    private Room current;
    private RoomPK currentPK;
    private DataModel<Room> items = null;
    private int selectedItemIndex;
    private boolean isCreate;

    public boolean isIsCreate() {
        return isCreate;
    }

    public void setIsCreate(boolean isCreate) {
        this.isCreate = isCreate;
    }

//</editor-fold>
    public RoomMB() {
    }

//<editor-fold defaultstate="collapsed" desc="Private Method">
    // Cái này em dùng như vầy là để lúc a copy qua thằng khác chỉ cần sửa cái return, mấy hàm phía dưới ko bị ảnh hưởng
    private IRoomRep getAction() {
        return ir;
    }

    // Tạo Model
    private DataModel createModel() {
        return new ListDataModel(getAction().findAll());
    }

    // Gọi hàm này thì items = null mà ở page jsf gọi thằng getItems() thằng này sẽ check if null thì khởi tạo.
    private void recreateModel() {
        items = null;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Public Method">
    public List<Room> getAll(){
        return getAction().findAll();
    }
    
    public DataModel getItems() {
        if (items == null) {
            items = createModel();
        }
        return items;
    }

    public Room getByID(Object id) {
        return getAction().find(id);
    }

    public Room getSelected() {
        if (current == null) {
            current = new Room();
            currentPK = new RoomPK();
            current.setRoomPK(currentPK);
            selectedItemIndex = -1;
        }
        return current;
    }

    public String prepareList() {
        recreateModel();
        return "Room?faces-redirect=true";
    }

    public String prepareCreate() {
        current = new Room();
        currentPK = new RoomPK();
        isCreate = true;
        current.setRoomPK(currentPK);
        return "RoomForm";
    }

    public String prepareEdit(Object id) {
        current = getByID(id);
        currentPK = getByID(id).getRoomPK();
        isCreate = false;
        return "RoomForm";
    }

    public String create() {
        try {
            current.setRoomPK(currentPK);
            current.setDepartment(id.find(current.getRoomPK().getDepartmentID()));
            getAction().create(current);
            JsfUtil.addSuccessMessage("Thêm thành công");
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, "Thêm thất bại");
            return null;
        }
    }

    public String update() {
        try {
            getAction().edit(current);
            current.setDepartment(id.find(current.getRoomPK().getDepartmentID()));
            JsfUtil.addSuccessMessage("Cập nhật thành công");
            return prepareEdit(current.getRoomPK());
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, "Cập nhật thất bại");
            return null;
        }
    }

    public Collection<Department> getRefDepartment() {
        return id.findAll();
    }  
//</editor-fold>
}
