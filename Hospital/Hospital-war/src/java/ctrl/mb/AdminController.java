package ctrl.mb;

import ctrl.mb.util.JsfUtil;
import ctrl.rep.interfaces.IAdminRep;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.view.facelets.FaceletContext;
import model.entities.Admin;

@Named(value = "adminController")
@SessionScoped
public class AdminController implements Serializable {
    
    @EJB
    private IAdminRep adminRep;
    private DataModel<Admin> items = null;
    private int selectedItemIndex;
    private Admin current;
    
    public AdminController() {
    }
    
    public Admin getAdmin() {
        if (current == null) {
            current = new Admin();
        }
        return current;
    }
    
    public void setAdmin(Admin admin) {
        this.current = admin;
    }

//<editor-fold defaultstate="collapsed" desc="Private Method">
    private IAdminRep getAction() {
        return adminRep;
    }
    
    private DataModel createModel() {
        return new ListDataModel(getAction().findAll());
    }
    
    private void recreateModel() {
        items = null;
    }

    private void performDestroy() {
        try {
            getAction().remove(current);
            JsfUtil.addSuccessMessage("Delete Successful !");
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, "Delete failed !");
        }
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Public Method">
    public String login() {
        Boolean resultLogin = adminRep.checklog(current);
        if (!resultLogin) {
            JsfUtil.addErrorMessage("Unknown login, try again");
            current = new Admin();
            return null;
        } else {
            JsfUtil.addSession("admin", getAction().find(current.getUsername()));
            return "Index?faces-redirect=true";
        }
    }
    
    public String logout() {
        return "Login.xhtml";
    }
    
    public Object getInfo() {
        Object ob = FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("admin");
        return FacesContext.getCurrentInstance().getExternalContext().getSession(true);
    }
    
    public DataModel getItems() {
        if (items == null) {
            items = createModel();
        }
        return items;
    }
    
    public Admin getByID(Object id) {
        return getAction().find(id);
    }
    
    public Admin getSelected() {
        if (current == null) {
            current = new Admin();
            selectedItemIndex = -1;
        }
        return current;
    }
    
    public String prepareList() {
        recreateModel();
        return "Admin?faces-redirect=true";
    }
    
    public String prepareCreate() {
        current = new Admin();
        current.setRole(Boolean.FALSE);
        return "AdminForm";
    }
    
    public String create() {
        try {
            getAction().create(current);
            JsfUtil.addSuccessMessage("Thêm thành công");
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, "Thêm thất bại");
            return null;
        }
    }
    
    public String destroy(Object id) {
        current = getByID(id);
        performDestroy();
        recreateModel();
        return "List";
    }
    
    public void removSession() {
        JsfUtil.addSession("admin", null);
    }
//</editor-fold>
}
