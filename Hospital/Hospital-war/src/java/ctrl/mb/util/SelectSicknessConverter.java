package ctrl.mb.util;

import ctrl.rep.interfaces.ISickness;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import model.entities.Sickness;

@ManagedBean
public class SelectSicknessConverter implements Converter {

    @EJB
    private ISickness id;

    @Override
    public Object getAsObject(FacesContext context, UIComponent comp, String value) {
        Object o = null;
        if (!(value == null || value.isEmpty())) {
            o = this.getSelectedItemAsEntity(comp, value);
        }
        return o;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        String s = "";
        if (value != null) {
            s = ((Sickness) value).getSicknessID().toString();
        }
        return s;
    }

    private Sickness getSelectedItemAsEntity(UIComponent comp, String value) {
        return id.find(Integer.valueOf(value));
    }

}
