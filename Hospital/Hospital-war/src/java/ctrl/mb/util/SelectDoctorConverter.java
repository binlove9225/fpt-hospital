/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ctrl.mb.util;

import ctrl.rep.interfaces.IEmployeeRep;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.NoneScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import model.entities.Employees;

/**
 *
 * @author lenovo
 */
@ManagedBean
@NoneScoped
public class SelectDoctorConverter  implements Converter{
    @EJB
    private IEmployeeRep id;
    
    public SelectDoctorConverter() {
    }
    
    @Override
    public Object getAsObject(FacesContext ctx, UIComponent comp, String value) {
        Object o = null;
        if (!(value == null || value.isEmpty())) {
            o = this.getSelectedItemAsEntity(comp, value);
        }
        return o;
    }

     @Override
    public String getAsString(FacesContext ctx, UIComponent comp, Object value) {
        String s = "";
        if (value != null) {
            s = ((Employees) value).getEmployeeID().toString();
        }
        return s;
    }

    private Employees getSelectedItemAsEntity(UIComponent comp, String value) {
        return id.find(Integer.valueOf(value));
    }
    
}
