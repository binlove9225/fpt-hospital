package ctrl.mb.util;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import javax.faces.model.DataModel;

public abstract class PaginationHelper {

    private int pageSize;
    private int page;

    public PaginationHelper(int pageSize) {
        this.pageSize = pageSize;
    }

    public abstract int getItemsCount();

    public abstract DataModel createPageDataModel();
    
    public abstract DataModel createPageDataModelWithSearch(String name);

    public int getPageFirstItem() {
        return page * pageSize ;
    }

    public int getPageLastItem() {
        int i = getPageFirstItem() + pageSize - 1;
        int count = getItemsCount() - 1;
        if (i > count) {
            i = count;
        }
        if (i < 0) {
            i = 0;
        }
        return i;
    }

    public int getPage() {
        return page + 1;
    }

    public List<Integer> getCountPage() {
        int countPage = getItemsCount() / pageSize;
        countPage = getItemsCount() % pageSize == 0 ? countPage - 1 : countPage;
        List<Integer> arr = new ArrayList<>();
        for (int i = 0; i <= countPage; i++) {
            arr.add(i + 1);
        }
        return arr;
    }

    public boolean isHasNextPage() {
        return (page + 1) * pageSize + 1 <= getItemsCount();
    }

    public void joinPage(int page) {
        this.page = page - 1;
    }

    public void nextPage() {
        if (isHasNextPage()) {
            page++;
        }
    }

    public boolean isHasPreviousPage() {
        return page > 0;
    }

    public void previousPage() {
        if (isHasPreviousPage()) {
            page--;
        }
    }

    public int getPageSize() {
        return pageSize;
    }

}
