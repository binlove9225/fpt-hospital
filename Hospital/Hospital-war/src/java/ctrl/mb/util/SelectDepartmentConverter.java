package ctrl.mb.util;

import ctrl.rep.implement.DepartmentRep;
import ctrl.rep.interfaces.IDepartmentRep;
import javax.ejb.EJB;
import javax.faces.bean.*;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.*;
import model.entities.Department;

@ManagedBean
public class SelectDepartmentConverter implements Converter {

    @EJB
    private IDepartmentRep id;

    @Override
    public Object getAsObject(FacesContext ctx, UIComponent comp, String value) {
        Object o = null;
        if (!(value == null || value.isEmpty())) {
            o = this.getSelectedItemAsEntity(comp, value);
        }
        return o;
    }

     @Override
    public String getAsString(FacesContext ctx, UIComponent comp, Object value) {
        String s = "";
        if (value != null) {
            s = ((Department) value).getDepartmentID().toString();
        }
        return s;
    }

    private Department getSelectedItemAsEntity(UIComponent comp, String value) {
        return id.find(Integer.valueOf(value));
    }
}
