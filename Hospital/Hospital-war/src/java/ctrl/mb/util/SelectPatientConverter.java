package ctrl.mb.util;

import ctrl.rep.interfaces.IPatientRep;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import model.entities.Patient;


@ManagedBean
public class SelectPatientConverter implements Converter{
    @EJB
    private IPatientRep id;


    @Override
    public Object getAsObject(FacesContext context, UIComponent comp, String value) {
        Object o = null;
        if (!(value == null || value.isEmpty())) {
            o = this.getSelectedItemAsEntity(comp, value);
        }
        return o;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        String s = "";
        if (value != null) {
            s = ((Patient) value).getPatientID().toString();
        }
        return s;
    }
    
    private Patient getSelectedItemAsEntity(UIComponent comp, String value) {
        return id.find(Integer.valueOf(value));
    }
    
}
