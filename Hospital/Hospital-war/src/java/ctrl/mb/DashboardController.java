package ctrl.mb;

import ctrl.rep.interfaces.*;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.Dependent;

@Named(value = "dashboardController")
@Dependent
public class DashboardController {

    @EJB
    private IEmployeeRep ie;
    @EJB
    private IPatientRep ip;
    @EJB
    private IMedicineRep im;
    @EJB
    private IMedicalReportRep imr;
    @EJB
    private IAppointmentRep ia;

    public DashboardController() {
    }

    public int getCountDoctor() {
        return ie.countDoctor();
    }

    public int getCountStaff() {
        return ie.countStaff();
    }

    public int getCountPatient() {
        return ip.count();
    }

    public int getCountMedicine() {
        return im.count();
    }

    public int getCountMedicalReport() {
        return imr.count();
    }

    public int getCountAppointmentWaiting() {
        return ia.countAppointmentWaiting();
    }

    public int getCountAppointmentFinish() {
        return ia.countAppointmentFinish();
    }

    public String prepareList() {
        return "Index?faces-redirect=true";
    }
}
