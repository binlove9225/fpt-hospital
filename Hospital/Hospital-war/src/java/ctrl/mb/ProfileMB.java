package ctrl.mb;

import static com.sun.faces.facelets.util.Path.context;
import ctrl.mb.util.JsfUtil;
import ctrl.rep.interfaces.*;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.*;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.model.*;
import javax.faces.validator.ValidatorException;
import lib.helper.EncryptHelp;

import model.entities.*;

@ManagedBean
@SessionScoped
public class ProfileMB {

//<editor-fold defaultstate="collapsed" desc="Field">
    @EJB
    private IEmployeeRep ie;
    @EJB
    private IDepartmentRep id;
    private Employees current;
    private boolean isChangeLogin = false;

    public boolean isIsChangeLogin() {
        return isChangeLogin;
    }

    public void setIsChangeLogin(boolean isChangeLogin) {
        this.isChangeLogin = isChangeLogin;
    }

//</editor-fold>
//<editor-fold defaultstate="collapsed" desc="Constructor">
    public ProfileMB() {
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Private Method">
    private IEmployeeRep getAction() {
        return ie;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Public Method">
    public Employees getByID(Object id) {
        return getAction().find(id);
    }

    public Employees getSelected() {
        if (current == null) {
            current = new Employees();
        }
        return current;
    }

    public String prepareEditProfile(Object id) {
        current = getByID(id);
        isChangeLogin = false;
        return "Profile";
    }

    public void prepareEditLogin() {
        isChangeLogin = isChangeLogin ? false : true;
    }

    public String update() {
        try {
            getAction().edit(current);
            JsfUtil.addSuccessMessage("Cập nhật thành công");
            JsfUtil.addSession("employee", getByID(current.getEmployeeID()));
            return prepareEditProfile(current.getEmployeeID());
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, "Cập nhật thất bại");
            return null;
        }
    }

    public Collection<Department> getRefDepartment() {
        return id.findAll();
    }

    public Collection<Employees> getRefemployeeses() {
        return ie.findAllDoctor();
    }

    public void validateCheckPassword(FacesContext context, UIComponent toValidate, Object value) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        String oldPassword = (String) value;
        oldPassword = EncryptHelp.Encrypt(oldPassword);
        if (!oldPassword.equals(getByID(current.getEmployeeID()).getPassword())) {
            FacesMessage message;
            message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Old Password Incorrect!", "Old Password Incorrect!");
            throw new ValidatorException(message);
        }
    }

    public void validateSamePassword(FacesContext context, UIComponent toValidate, Object value) {
        String confirmPassword = (String) value;
        if (!confirmPassword.equals(current.getPassword())) {
            FacesMessage message;
            message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Confirm Password do not match!", "Confirm Password do not match!");
            throw new ValidatorException(message);
        }
    }

//</editor-fold>
}
