/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ctrl.mb;

import ctrl.mb.util.JsfUtil;
import ctrl.rep.implement.EmployeeRep;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import model.entities.Employees;
import org.json.simple.JSONObject;

/**
 *
 * @author lenovo
 */
@ManagedBean
@SessionScoped
public class ScheduleMB {

    @EJB
    private EmployeeRep employeeRep;

    Read_Write_Json json = new Read_Write_Json();
    private String filename;
    private String scheduleName;
    private String date;
    private String time;
    private int doctor;
    private int room;
    private int patientId;
    private String search_date;
    private String search_department;
    public  String path="\\ctrl\\json\\schedule";
    
    public ScheduleMB() {
    }

    public List<String> getName() {
        File folder = new File(path);
        File[] listOfFiles = folder.listFiles();
        List<String> list = new ArrayList<>();
        for (int i = 0; i < listOfFiles.length; i++) {
            list.add(listOfFiles[i].getName().substring(0, listOfFiles[i].getName().length() - 5));
        }
        return list;
    }

    public void search_schedule() {
        getAll();
    }

    public List<Object> search() {
        File file = new File(path);
        List<Object> list = json.read_json(file.getPath() + "\\Schedule.json");
        List<Object> arraList = new ArrayList<>();
        if (search_date != null && search_department != null) {
            for (int i = 0; i < list.size(); i++) {
                JSONObject obj = (JSONObject) list.get(i);
                if (obj.get("date").toString().equals(search_date) && obj.get("department").toString().equals(search_department)) {
                    arraList.add((Object) obj);
                }
            }
        }
        if (search_date != null&&search_department==null||search_department.equals("")) {
            for (int i = 0; i < list.size(); i++) {
                JSONObject obj = (JSONObject) list.get(i);
                if (obj.get("date").toString().equals(search_date)) {
                    arraList.add((Object) obj);
                }
            }
        }
        if (search_department != null&&search_date==null||search_date.equals("")) {
            for (int i = 0; i < list.size(); i++) {
                JSONObject obj = (JSONObject) list.get(i);
                if (obj.get("department").toString().equals(search_department)) {
                    arraList.add((Object) obj);
                }
            }
        }
        return arraList;
    }

    public List<Object> getAll() {
        File file = new File(path);
        if (search_date != null&&search_date.equals("") || search_department != null&&!search_department.equals("")) {
            return search();
        }
        return json.read_json(file.getPath()+"\\Schedule.json");
    }

    public String create() {
        JSONObject obj = new JSONObject();
        Employees emp = employeeRep.find(doctor);
        obj.put("date", date);
        obj.put("time", time);
        obj.put("doctorId", doctor);
        obj.put("department", emp.getDepartmentID().getDepartmentID());
        obj.put("room", room);
        obj.put("status", "true");
        List<Object> list= json.read_json("schedule\\Schedule");
        for (int i = 0; i < list.size(); i++) {
            JSONObject o= (JSONObject)list.get(i);
            if(o.get("date").equals(obj.get("date")) && o.get("time").equals(obj.get("time"))){
            JsfUtil.addSuccessMessage("Thêm Lịch thất bại");
            return prepareCreate();
            }
        }
        json.add_schedule("schedule\\Schedule", obj);
        return "Schedule.xhtml";
    }

    public String prepareCreate() {
        return "ScheduleForm?faces-redirect=true";
    }
    
    public String prepareList() {
        return "Schedule?faces-redirect=true";
    }
    
    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getScheduleName() {
        return scheduleName;
    }

    public void setScheduleName(String scheduleName) {
        this.scheduleName = scheduleName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getDoctor() {
        return doctor;
    }

    public void setDoctor(int doctor) {
        this.doctor = doctor;
    }

    public int getRoom() {
        return room;
    }

    public void setRoom(int room) {
        this.room = room;
    }

    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    public String getSearch_date() {
        return search_date;
    }

    public void setSearch_date(String search_date) {
        this.search_date = search_date;
    }

    public String getSearch_department() {
        return search_department;
    }

    public void setSearch_department(String search_department) {
        this.search_department = search_department;
    }

}
