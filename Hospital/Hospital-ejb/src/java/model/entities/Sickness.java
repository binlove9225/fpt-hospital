/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Asus
 */
@Entity
@Table(name = "Sickness", catalog = "hospital", schema = "dbo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Sickness.findAll", query = "SELECT s FROM Sickness s")
    , @NamedQuery(name = "Sickness.findBySicknessID", query = "SELECT s FROM Sickness s WHERE s.sicknessID = :sicknessID")
    , @NamedQuery(name = "Sickness.findBySicknessName", query = "SELECT s FROM Sickness s WHERE s.sicknessName = :sicknessName")})
public class Sickness implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "SicknessID")
    private Integer sicknessID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "SicknessName")
    private String sicknessName;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sicknessID")
    private Collection<MedicalReport> medicalReportCollection;

    public Sickness() {
    }

    public Sickness(Integer sicknessID) {
        this.sicknessID = sicknessID;
    }

    public Sickness(Integer sicknessID, String sicknessName) {
        this.sicknessID = sicknessID;
        this.sicknessName = sicknessName;
    }

    public Integer getSicknessID() {
        return sicknessID;
    }

    public void setSicknessID(Integer sicknessID) {
        this.sicknessID = sicknessID;
    }

    public String getSicknessName() {
        return sicknessName;
    }

    public void setSicknessName(String sicknessName) {
        this.sicknessName = sicknessName;
    }

    @XmlTransient
    public Collection<MedicalReport> getMedicalReportCollection() {
        return medicalReportCollection;
    }

    public void setMedicalReportCollection(Collection<MedicalReport> medicalReportCollection) {
        this.medicalReportCollection = medicalReportCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sicknessID != null ? sicknessID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sickness)) {
            return false;
        }
        Sickness other = (Sickness) object;
        if ((this.sicknessID == null && other.sicknessID != null) || (this.sicknessID != null && !this.sicknessID.equals(other.sicknessID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.entities.Sickness[ sicknessID=" + sicknessID + " ]";
    }
    
}
