/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Asus
 */
@Entity
@Table(name = "Prescription", catalog = "hospital", schema = "dbo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Prescription.findAll", query = "SELECT p FROM Prescription p")
    , @NamedQuery(name = "Prescription.findByPrescriptionID", query = "SELECT p FROM Prescription p WHERE p.prescriptionID = :prescriptionID")
    , @NamedQuery(name = "Prescription.findByMorning", query = "SELECT p FROM Prescription p WHERE p.morning = :morning")
    , @NamedQuery(name = "Prescription.findByAfternoon", query = "SELECT p FROM Prescription p WHERE p.afternoon = :afternoon")
    , @NamedQuery(name = "Prescription.findByEvening", query = "SELECT p FROM Prescription p WHERE p.evening = :evening")
    , @NamedQuery(name = "Prescription.findByTotal", query = "SELECT p FROM Prescription p WHERE p.total = :total")
    , @NamedQuery(name = "Prescription.findByNote", query = "SELECT p FROM Prescription p WHERE p.note = :note")})
public class Prescription implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "PrescriptionID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer prescriptionID;
    @Column(name = "Morning")
    private Boolean morning;
    @Column(name = "Afternoon")
    private Boolean afternoon;
    @Column(name = "Evening")
    private Boolean evening;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "Total")
    private Double total;
    @Size(max = 2147483647)
    @Column(name = "Note")
    private String note;
    @JoinColumn(name = "ReportID", referencedColumnName = "ReportID")
    @ManyToOne(optional = false)
    private MedicalReport reportID;
    @JoinColumn(name = "MedicineID", referencedColumnName = "MedicineID")
    @ManyToOne(optional = false)
    private Medicine medicineID;

    public Prescription() {
        this.morning = false;
        this.afternoon = false;
        this.evening = false;
        this.prescriptionID = 0;
    }

    public Prescription(Integer prescriptionID) {
        this.prescriptionID = prescriptionID;
    }

    public Integer getPrescriptionID() {
        return prescriptionID;
    }

    public void setPrescriptionID(Integer prescriptionID) {
        this.prescriptionID = prescriptionID;
    }

    public Boolean getMorning() {
        return morning;
    }

    public void setMorning(Boolean morning) {
        this.morning = morning;
    }

    public Boolean getAfternoon() {
        return afternoon;
    }

    public void setAfternoon(Boolean afternoon) {
        this.afternoon = afternoon;
    }

    public Boolean getEvening() {
        return evening;
    }

    public void setEvening(Boolean evening) {
        this.evening = evening;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public MedicalReport getReportID() {
        return reportID;
    }

    public void setReportID(MedicalReport reportID) {
        this.reportID = reportID;
    }

    public Medicine getMedicineID() {
        return medicineID;
    }

    public void setMedicineID(Medicine medicineID) {
        this.medicineID = medicineID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (prescriptionID != null ? prescriptionID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Prescription)) {
            return false;
        }
        Prescription other = (Prescription) object;
        if ((this.prescriptionID == null && other.prescriptionID != null) || (this.prescriptionID != null && !this.prescriptionID.equals(other.prescriptionID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.entities.Prescription[ prescriptionID=" + prescriptionID + " ]";
    }

}
