package model.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;


@Embeddable
public class RoomPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "RoomNo")
    private int roomNo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DepartmentID")
    private int departmentID;

    public RoomPK() {
    }

    public RoomPK(int roomNo, int departmentID) {
        this.roomNo = roomNo;
        this.departmentID = departmentID;
    }

    public int getRoomNo() {
        return roomNo;
    }

    public void setRoomNo(int roomNo) {
        this.roomNo = roomNo;
    }

    public int getDepartmentID() {
        return departmentID;
    }

    public void setDepartmentID(int departmentID) {
        this.departmentID = departmentID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) roomNo;
        hash += (int) departmentID;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RoomPK)) {
            return false;
        }
        RoomPK other = (RoomPK) object;
        if (this.roomNo != other.roomNo) {
            return false;
        }
        if (this.departmentID != other.departmentID) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.entities.RoomPK[ roomNo=" + roomNo + ", departmentID=" + departmentID + " ]";
    }
    
}
