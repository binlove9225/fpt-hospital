/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Asus
 */
@Entity
@Table(name = "MedicalReport", catalog = "hospital", schema = "dbo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MedicalReport.findAll", query = "SELECT m FROM MedicalReport m")
    , @NamedQuery(name = "MedicalReport.findByReportID", query = "SELECT m FROM MedicalReport m WHERE m.reportID = :reportID")
    , @NamedQuery(name = "MedicalReport.findByNote", query = "SELECT m FROM MedicalReport m WHERE m.note = :note")
    , @NamedQuery(name = "MedicalReport.findByDate", query = "SELECT m FROM MedicalReport m WHERE m.date = :date")})
public class MedicalReport implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ReportID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer reportID;
    @Size(max = 2147483647)
    @Column(name = "Note")
    private String note;
    @Basic(optional = false)
    @Column(name = "Date")
    @Temporal(TemporalType.DATE)
    private Date date;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "reportID")
    private Collection<Prescription> prescriptionCollection;
    @JoinColumn(name = "DoctorID", referencedColumnName = "EmployeeID")
    @ManyToOne(optional = false)
    private Employees doctorID;
    @JoinColumn(name = "PatientID", referencedColumnName = "PatientID")
    @ManyToOne(optional = false)
    private Patient patientID;
    @JoinColumn(name = "SicknessID", referencedColumnName = "SicknessID")
    @ManyToOne(optional = false)
    private Sickness sicknessID;

    public MedicalReport() {
    }

    public MedicalReport(Integer reportID) {
        this.reportID = reportID;
    }

    public MedicalReport(Integer reportID, Date date) {
        this.reportID = reportID;
        this.date = date;
    }

    public Integer getReportID() {
        return reportID;
    }

    public void setReportID(Integer reportID) {
        this.reportID = reportID;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @XmlTransient
    public Collection<Prescription> getPrescriptionCollection() {
        return prescriptionCollection;
    }

    public void setPrescriptionCollection(Collection<Prescription> prescriptionCollection) {
        this.prescriptionCollection = prescriptionCollection;
    }

    public Employees getDoctorID() {
        return doctorID;
    }

    public void setDoctorID(Employees doctorID) {
        this.doctorID = doctorID;
    }

    public Patient getPatientID() {
        return patientID;
    }

    public void setPatientID(Patient patientID) {
        this.patientID = patientID;
    }

    public Sickness getSicknessID() {
        return sicknessID;
    }

    public void setSicknessID(Sickness sicknessID) {
        this.sicknessID = sicknessID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (reportID != null ? reportID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MedicalReport)) {
            return false;
        }
        MedicalReport other = (MedicalReport) object;
        if ((this.reportID == null && other.reportID != null) || (this.reportID != null && !this.reportID.equals(other.reportID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.entities.MedicalReport[ reportID=" + reportID + " ]";
    }

}
