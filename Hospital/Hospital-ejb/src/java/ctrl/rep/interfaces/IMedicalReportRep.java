package ctrl.rep.interfaces;

import ctrl.rep.base.*;
import java.util.List;
import javax.ejb.Local;
import model.entities.*;

@Local
public interface IMedicalReportRep extends IBaseRep<MedicalReport> {

    List<MedicalReport> findByPatient(Patient entity);
}
