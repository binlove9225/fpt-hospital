package ctrl.rep.interfaces;

import ctrl.rep.base.*;
import java.util.List;
import javax.ejb.Local;
import model.entities.*;

@Local
public interface IAppointmentRep extends IBaseRep<Appointment> {

    int countAppointmentWaiting();

    int countAppointmentFinish();

    int findByDateTime(String date, String time, Employees doctorID);

    List<Appointment> findByDepartment(Object id, String status);

    List<Appointment> findByDoctor(Object id, String status);
    
    int findByRoom(int roomNo, String date);
    
    int findbyDepartment(int departmentID, String date);
    
    List<String> getTime(String date,Employees doctorId);
    
}
