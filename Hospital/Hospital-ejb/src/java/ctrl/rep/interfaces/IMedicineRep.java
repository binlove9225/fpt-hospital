package ctrl.rep.interfaces;

import ctrl.rep.base.*;
import javax.ejb.Local;
import model.entities.*;

@Local
public interface IMedicineRep extends IBaseRep<Medicine> {

}
