
package ctrl.rep.interfaces;

import ctrl.rep.base.IBaseRep;
import javax.ejb.Local;
import model.entities.Patient;

@Local
public interface IPatientRep extends IBaseRep<Patient>{
    boolean checkCMND(String cmnd);
}
