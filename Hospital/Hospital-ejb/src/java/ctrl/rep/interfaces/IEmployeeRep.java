package ctrl.rep.interfaces;

import ctrl.rep.base.*;
import java.util.List;
import javax.ejb.Local;
import model.entities.*;

@Local
public interface IEmployeeRep extends IBaseRep<Employees> {

    int countDoctor();

    int countStaff();

    List<Employees> findAllDoctor();

    List<Employees> findAffStaff();

    List<Employees> checkLogin();

    boolean checkEmployeeEmail(String empEmail);

    String changePassword(int employeeID, String password);

    String changeEmployeeStatus(int employeeID, int status);

    Employees checklog(Employees entity);

}
