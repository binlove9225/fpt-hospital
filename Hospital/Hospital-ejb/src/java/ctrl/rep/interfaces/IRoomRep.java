/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ctrl.rep.interfaces;

import ctrl.rep.base.IBaseRep;
import javax.ejb.Local;
import model.entities.Room;

@Local
public interface IRoomRep extends IBaseRep<Room> {
    
}
