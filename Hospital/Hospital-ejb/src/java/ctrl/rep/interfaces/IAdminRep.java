package ctrl.rep.interfaces;

import ctrl.rep.base.IBaseRep;
import javax.ejb.Local;
import model.entities.Admin;

@Local
public interface IAdminRep extends IBaseRep<Admin> {

    boolean checklog(Admin admin);
}
