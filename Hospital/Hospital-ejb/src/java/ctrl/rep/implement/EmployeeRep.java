package ctrl.rep.implement;

import ctrl.rep.base.BaseRep;
import ctrl.rep.interfaces.IEmployeeRep;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.*;
import javax.persistence.*;
import lib.helper.EncryptHelp;
import model.entities.*;

@Stateless
@LocalBean
public class EmployeeRep extends BaseRep<Employees> implements IEmployeeRep {

//<editor-fold defaultstate="collapsed" desc="Fields">
    @PersistenceContext(name = "Hospital-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Constructor">
    public EmployeeRep() {
        super(Employees.class);
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Method">
    @Override
    public int countDoctor() {
        return getEntityManager()
                .createQuery("SELECT e FROM Employees e WHERE e.isDoctor = 1")
                .getResultList()
                .size();
    }

    @Override
    public int countStaff() {
        return getEntityManager()
                .createQuery("SELECT e FROM Employees e WHERE e.isDoctor = 0")
                .getResultList()
                .size();
    }

    @Override
    public List<Employees> findAllDoctor() {
        return getEntityManager()
                .createQuery("SELECT e FROM Employees e WHERE e.isDoctor = 1")
                .getResultList();
    }

    @Override
    public List<Employees> findAffStaff() {
        return getEntityManager()
                .createQuery("SELECT e FROM Employees e WHERE e.isDoctor = 0")
                .getResultList();
    }

    @Override
    public List<Employees> checkLogin() {
        Query q = em.createNativeQuery("select * from Employees", Employees.class);
        return q.getResultList();
    }

    @Override
    public boolean checkEmployeeEmail(String empEmail) {
        return getEntityManager()
                .createQuery("SELECT e FROM Employees e WHERE e.email= :email AND e.status = 1")
                .setParameter("email", empEmail)
                .getResultList()
                .size() > 0;
    }

    @Override
    public String changePassword(int employeeID, String password) {
        try {
            em.createNativeQuery("update Employees set Password = ? where EmployeeID = ?")
                    .setParameter(1, password)
                    .setParameter(2, employeeID)
                    .executeUpdate();
            return "yes";
        } catch (Exception e) {
            return String.valueOf(e);
        }
    }

    @Override
    public String changeEmployeeStatus(int employeeID, int status) {
        try {
            Query q1 = em.createNativeQuery("update Employees set Status = ? where EmployeeID = ?");
            q1.setParameter(1, status);
            q1.setParameter(2, employeeID);
            q1.executeUpdate();
            return "yes";

        } catch (Exception e) {
            return String.valueOf(e);
        }
    }

    @Override
    public Employees checklog(Employees entity) {
        try {
            String passEncrypt = EncryptHelp.Encrypt(entity.getPassword());
            entity.setPassword(passEncrypt);
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
            Logger.getLogger(AdminRep.class.getName()).log(Level.SEVERE, null, ex);
        }
        Query q = getEntityManager()
                .createQuery("SELECT A FROM Employees A WHERE A.email = :user AND A.password = :pass AND A.status = 1")
                .setParameter("user", entity.getEmail())
                .setParameter("pass", entity.getPassword());

        if (q.getResultList().size() > 0) {
            return (Employees) q.getSingleResult();
        }
        return null;
    }

    @Override
    public boolean create(Employees entity) {
        try {
            String passEncrypt = EncryptHelp.Encrypt(entity.getPassword());
            entity.setPassword(passEncrypt);
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
            Logger.getLogger(AdminRep.class.getName()).log(Level.SEVERE, null, ex);
        }
        return super.create(entity);
    }

    @Override
    public boolean edit(Employees entity) {
        try {
            String passEncrypt = EncryptHelp.Encrypt(entity.getPassword());
            entity.setPassword(passEncrypt);
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
            Logger.getLogger(AdminRep.class.getName()).log(Level.SEVERE, null, ex);
        }
        return super.edit(entity);
    }
//</editor-fold>

}
