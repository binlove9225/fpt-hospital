package ctrl.rep.implement;

import ctrl.rep.base.BaseRep;
import ctrl.rep.interfaces.IAdminRep;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import lib.helper.EncryptHelp;
import model.entities.Admin;

@Stateless
@LocalBean
public class AdminRep extends BaseRep<Admin> implements IAdminRep {

    //<editor-fold defaultstate="collapsed" desc="Fields">
    @PersistenceContext(name = "Hospital-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
//</editor-fold>

    public AdminRep() {
        super(Admin.class);
    }

    @Override
    public boolean checklog(Admin admin) {
        try {
            String passEncrypt = EncryptHelp.Encrypt(admin.getPassword());
            admin.setPassword(passEncrypt);
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
            Logger.getLogger(AdminRep.class.getName()).log(Level.SEVERE, null, ex);
        }

        return getEntityManager()
                .createQuery("SELECT A FROM Admin A WHERE A.username = :user AND A.password = :pass")
                .setParameter("user", admin.getUsername())
                .setParameter("pass", admin.getPassword())
                .getResultList()
                .size() > 0;
    }

    @Override
    public boolean create(Admin entity) {
        try {
            String passEncrypt = EncryptHelp.Encrypt(entity.getPassword());
            entity.setPassword(passEncrypt);
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
            Logger.getLogger(AdminRep.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return super.create(entity);
    }

}
