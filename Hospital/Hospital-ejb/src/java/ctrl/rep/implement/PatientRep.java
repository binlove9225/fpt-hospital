package ctrl.rep.implement;

import ctrl.rep.base.BaseRep;
import ctrl.rep.interfaces.IPatientRep;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import model.entities.Employees;
import model.entities.Patient;

@Stateless
@LocalBean
public class PatientRep extends BaseRep<Patient> implements IPatientRep {

    //<editor-fold defaultstate="collapsed" desc="Fields">
    @PersistenceContext(name = "Hospital-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Constructor">
    public PatientRep() {
        super(Patient.class);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Method">  
    @Override
    public boolean checkCMND(String cmnd) {
        return getEntityManager()
                .createNamedQuery("Patient.findByCmnd")
                .setParameter("cmnd", cmnd)
                .getResultList().size() > 0;
    }
    //</editor-fold>

}
