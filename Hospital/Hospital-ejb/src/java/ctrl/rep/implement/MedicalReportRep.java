package ctrl.rep.implement;

import ctrl.rep.base.BaseRep;
import ctrl.rep.interfaces.*;
import java.util.List;
import javax.ejb.*;
import javax.persistence.*;
import model.entities.*;

@Stateless
@LocalBean
public class MedicalReportRep extends BaseRep<MedicalReport> implements IMedicalReportRep {

//<editor-fold defaultstate="collapsed" desc="Fields">
    @PersistenceContext(name = "Hospital-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Constructor">
    public MedicalReportRep() {
        super(MedicalReport.class);
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Method">
    @Override
    public List<MedicalReport> findByPatient(Patient entity) {
        return getEntityManager()
                .createQuery("SELECT m FROM MedicalReport m WHERE m.patientID = :patientID")
                .setParameter("patientID", entity)
                .getResultList();
    }
//</editor-fold>

}
