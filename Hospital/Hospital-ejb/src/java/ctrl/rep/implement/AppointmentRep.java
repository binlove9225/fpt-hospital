package ctrl.rep.implement;

import ctrl.rep.base.BaseRep;
import ctrl.rep.interfaces.*;
import java.util.List;
import javax.ejb.*;
import javax.persistence.*;
import model.entities.*;

@Stateless
@LocalBean
public class AppointmentRep extends BaseRep<Appointment> implements IAppointmentRep {

//<editor-fold defaultstate="collapsed" desc="Fields">
    @PersistenceContext(name = "Hospital-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Constructor">
    public AppointmentRep() {
        super(Appointment.class);
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Method">
    @Override
    public int countAppointmentWaiting() {
        return getEntityManager()
                .createNamedQuery("Appointment.findByStatus")
                .setParameter("status", "NotComplete")
                .getResultList()
                .size();
    }

    @Override
    public int countAppointmentFinish() {
        return getEntityManager()
                .createNamedQuery("Appointment.findByStatus")
                .setParameter("status", "Completed")
                .getResultList()
                .size();
    }

    @Override
    public int findByDateTime(String date, String time, Employees doctorID) {
        return getEntityManager().createQuery("select a from Appointment a where a.date=:date and a.time = :time and a.doctorID = :doctorID")
                .setParameter("date", date)
                .setParameter("time", time)
                .setParameter("doctorID", doctorID).getResultList().size();
    }

    @Override
    public List<Appointment> findByDepartment(Object id, String status) {
        return getEntityManager().createQuery("select a from Appointment a where a.doctorID is null and a.departmentID=:departmentID and a.status = :status")
                .setParameter("departmentID", id)
                .setParameter("status", status)
                .getResultList();
    }

    @Override
    public List<Appointment> findByDoctor(Object id, String status) {
        return getEntityManager().createQuery("select a from Appointment a where a.doctorID=:doctorID and a.status = :status")
                .setParameter("doctorID", id)
                .setParameter("status", status)
                .getResultList();
    }

    @Override
    public int findByRoom(int roomNo, String date) {
        return Integer.parseInt(getEntityManager().createQuery("select COUNT(a) FROM Appointment a where a.date = :date and a.roomNo = :roomNo")
                .setParameter("date", date)
                .setParameter("roomNo", roomNo)
                .getSingleResult().toString());
    }

    @Override
    public int findbyDepartment(int departmentID, String date) {
        return Integer.parseInt(getEntityManager().createQuery("select COUNT(a) FROM Appointment a where a.date = :date and a.departmentID = :department and a.time IS NULL")
                .setParameter("date", date)
                .setParameter("department", departmentID)
                .getSingleResult().toString());
    }

    @Override
    public List<String> getTime(String date, Employees doctorId) {
        return getEntityManager().createQuery("select a.time from Appointment a where a.date = :date and a.doctorID = :doctorId and a.time IS NOT NULL")
                .setParameter("date", date)
                .setParameter("doctorId", doctorId).getResultList();
    }

//</editor-fold>
}
