package ctrl.rep.implement;

import ctrl.rep.base.BaseRep;
import ctrl.rep.interfaces.IRoomRep;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import model.entities.Room;

@Stateless
@LocalBean
public class RoomRep extends BaseRep<Room> implements IRoomRep{

    //<editor-fold defaultstate="collapsed" desc="Fields">
    @PersistenceContext(name = "Hospital-ejbPU")
    private EntityManager em;
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
//</editor-fold>
    public RoomRep(){
        super(Room.class);
    }
    
//<editor-fold defaultstate="collapsed" desc="Method">
    @Override
    public boolean create(Room entity) {
        try {
            getEntityManager().persist(entity);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
    
    @Override
    public boolean edit(Room entity) {
        try {
            getEntityManager().merge(entity);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
//</editor-fold>
}
