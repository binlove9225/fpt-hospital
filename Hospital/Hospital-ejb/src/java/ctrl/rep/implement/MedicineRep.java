package ctrl.rep.implement;

import ctrl.rep.base.BaseRep;
import ctrl.rep.interfaces.*;
import java.util.List;
import javax.ejb.*;
import javax.persistence.*;
import model.entities.*;

@Stateless
@LocalBean
public class MedicineRep extends BaseRep<Medicine> implements IMedicineRep {

//<editor-fold defaultstate="collapsed" desc="Fields">
    @PersistenceContext(name = "Hospital-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Constructor">
    public MedicineRep() {
        super(Medicine.class);
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Method">
    
//</editor-fold>

}
