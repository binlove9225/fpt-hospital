/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ctrl.rep.implement;

import ctrl.rep.base.BaseRep;
import ctrl.rep.interfaces.IDepartmentRep;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import model.entities.Department;

/**
 *
 * @author tuansang
 */
@Stateless
@LocalBean
public class DepartmentRep extends BaseRep<Department> implements IDepartmentRep {

    //<editor-fold defaultstate="collapsed" desc="Fields">
    @PersistenceContext(name = "Hospital-ejbPU")
    private EntityManager em;
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
//</editor-fold>
    
    public DepartmentRep() {
        super(Department.class);
    }

    @Override
    public List<Department> getByDoctor() {
        return em.createNamedQuery("Department.findByIsDoctor").setParameter("isDoctor", true).getResultList();
    }
}
