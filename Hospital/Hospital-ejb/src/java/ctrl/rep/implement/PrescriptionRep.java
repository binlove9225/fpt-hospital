package ctrl.rep.implement;

import ctrl.rep.base.BaseRep;
import ctrl.rep.interfaces.IPrescriptionRep;
import java.util.Collection;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import model.entities.Prescription;

@Stateless
@LocalBean
public class PrescriptionRep extends BaseRep<Prescription> implements IPrescriptionRep {

    //<editor-fold defaultstate="collapsed" desc="Fields">
    @PersistenceContext(name = "Hospital-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
//</editor-fold>

    public PrescriptionRep() {
        super(Prescription.class);
    }

    @Override
    public void creates(Collection<Prescription> list) {
        list.forEach((prescription) -> {
            super.create(prescription);
        });
    }
}
