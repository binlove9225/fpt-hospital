package ctrl.rep.base;

import java.util.List;
import javax.persistence.*;
import javax.persistence.criteria.*;


public abstract class BaseRep<T> implements IBaseRep<T> {

//<editor-fold defaultstate="collapsed" desc="fields">
    private Class<T> entityClass;

    protected abstract EntityManager getEntityManager();
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="constructor">
    public BaseRep(Class<T> entityClass) {
        this.entityClass = entityClass;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="method">
    @Override
    public boolean create(T entity) {
        try {
            getEntityManager().persist(entity);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    @Override
    public boolean edit(T entity) {
        try {
            getEntityManager().merge(entity);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    @Override
    public boolean remove(T entity) {
        try {
            getEntityManager().remove(getEntityManager().merge(entity));
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    @Override
    public T find(Object id) {
        return getEntityManager().find(entityClass, id);
    }

    @Override
    public List<T> findAll() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        return getEntityManager().createQuery(cq).getResultList();
    }

    @Override
    public List<T> findRange(int[] range) {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0] + 1);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<T> rt = cq.from(entityClass);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
//</editor-fold>

}
