package ctrl.rep.base;

import java.util.List;

public interface IBaseRep<T> {

    boolean create(T entity);

    boolean edit(T entity);

    boolean remove(T entity);

    T find(Object id);

    List<T> findAll();

    List<T> findRange(int[] range);

    int count();    
}
