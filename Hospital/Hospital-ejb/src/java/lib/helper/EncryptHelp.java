package lib.helper;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.xml.bind.DatatypeConverter;

public class EncryptHelp {

    public static String Encrypt(String content) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(content.getBytes("UTF-8"));
        byte[] pass = md.digest();
        return DatatypeConverter.printBase64Binary(pass);
    }
    
}
